import java.util.*;
import java.util.function.BiFunction;

public class Main {

    public static void main(String[] args) {
        double totalCost = 0;

        BiFunction<String,Double,Product> createProduct = (productName, productPrice) -> {
            return new Product(productName,productPrice);
        };

        BiFunction<Product,Integer,Double> calculateCost =
                (product,quantity) -> product.productPrice * quantity;

        Product PS5 = createProduct.apply("PS5", 50000.0);
        Product xBoxX = createProduct.apply("XBox X", 45000.0);
        Product iPhone = createProduct.apply("iPhone", 80000.0);
        Product wirelessCharger = createProduct.apply("Charger", 1000.0);
        Product books = createProduct.apply("Comics", 500.0);
        Product lightningCable = createProduct.apply("Cable", 45.0);

        HashMap<Product,Integer> cart = new HashMap<>();
        cart.put(PS5,2);
        cart.put(xBoxX,1);
        cart.put(iPhone,2);
        cart.put(wirelessCharger,1);
        cart.put(books,4);
        cart.put(lightningCable,2);

        System.out.println("Products created using BiFunction");
        for (Map.Entry<Product,Integer> item: cart.entrySet()) {
            System.out.println(item.getKey().toString());
        }

        System.out.println("Total cost calculated using BiFunction");
        for (Map.Entry<Product,Integer> item: cart.entrySet()) {
            totalCost += calculateCost.apply(item.getKey(),item.getValue());
        }
        System.out.println("Total cost of cart : "+totalCost);
    }
}
