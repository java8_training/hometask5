public class Product {

    String productName, productCategory, productGrade;
    double productPrice;

    Product(String productName, double productPrice, String productCategory, String productGrade){
        this.productName = productName;
        this.productPrice = productPrice;
        this.productCategory = productCategory;
        this.productGrade = productGrade;
    }

    Product(String productName, double productPrice){
        this.productName = productName;
        this.productPrice = productPrice;
        this.productCategory = "Undefined";
        this.productGrade = "Undefined";
    }

    @Override
    public String toString(){
        return "Product Name : "+productName+"\tProduct Price : "+productPrice+"\tProduct Category : "+productCategory+"\tProduct Grade : "+productGrade;
    }

}
